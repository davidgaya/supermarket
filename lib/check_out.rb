class CheckOut

  def initialize(pricing_rules)
    @pricing_rules = pricing_rules
  end

  def scan(item)
    @items ||= {}
    @items[item] ||= 0
    @items[item] = @items[item] + 1
  end

  def total
    basket_total = 0
    @items.each do |item, total|
      basket_total += @pricing_rules.total(sku: item.sku, quantity: total)
    end
    basket_total
  end

end
