class Pound

  def initialize(quantity)
    @quantity = quantity
  end

  def to_f
    @quantity.to_f
  end

  def convert_to new_unit
    case new_unit
      when :pound, :pounds
        @quantity
      when :ounce, :ounces
        @quantity * 4
      else
        raise 'Unsupported Unit conversion'
    end

  end

end