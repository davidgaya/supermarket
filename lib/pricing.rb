class Pricing

  def self.factory(type, *options)
    #ToDo: consider converting symbol to class
    case type
      when :unit
        Unit.new(*options)
      when :bundled
        Bundled.new(*options)
      when :uncountable
        Uncountable.new(*options)
      when :get_extra_one
        GetExtraOne.new(*options)
      when :evented
        Evented.new(*options)
      else
        raise "Unsupported pricing type"
    end
  end

  def initialize(*options)
    @options = options.first
  end

  def total(quantity)
    raise NotImplementedError
  end

  def audit
    @options[:audit]
  end

  def log param
    if audit
      audit.info param
    end
  end

end

require 'pricing/unit'
require 'pricing/bundled'
require 'pricing/uncountable'
require 'pricing/get_extra_one'
require 'pricing/evented'
