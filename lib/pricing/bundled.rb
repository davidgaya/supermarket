class Pricing::Bundled < Pricing

  def bundle_number
    @options[:bundle]
  end

  def bundle_price
    @options[:price]
  end

  def unit_pricing
    @options[:unit_pricing]
  end

  #ToDo: consider making unit pricing mandatory to simplify this
  def total(quantity)
    bundles = quantity / bundle_number
    units = quantity % bundle_number
    unless unit_pricing.nil?
      bundled_total(bundles) + unit_pricing.total(units)
    else
      raise "Unconfigured unit pricing" if units > 0
      bundled_total(bundles)
    end
  end

  def bundled_total(bundles)
    log "Bundled: #{bundles} units @ #{bundle_price}€ = #{bundles * bundle_price}€"
    bundles * bundle_price
  end

end
