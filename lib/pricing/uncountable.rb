class Pricing::Uncountable < Pricing

  def unit_price
    @options[:price]
  end

  def per_unit
    @options[:per_unit]
  end

  def total(quantity)
    converted_quantity = quantity.convert_to per_unit
    converted_quantity * unit_price
  end

end