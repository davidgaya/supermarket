class Pricing::Evented < Pricing

  def start_date
    @options[:start_date]
  end

  def end_date
    @options[:end_date]
  end

  def pricing
    @options[:pricing]
  end

  def current_date
    @current_date ||= Date.today
  end

  def total quantity
    if current_date > start_date && current_date < end_date
      log("Evented: #{start_date}>#{end_date}")
      pricing.total(quantity)
    else
      raise "Evented pricing is not active"
    end
  end

end