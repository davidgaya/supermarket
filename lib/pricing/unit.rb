class Pricing::Unit < Pricing

  def unit_price
    @options[:price]
  end

  def total(quantity)
    log "#{quantity} units @ #{unit_price}€ = #{quantity * unit_price}€"
    quantity * unit_price
  end

end
