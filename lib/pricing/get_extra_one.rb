require 'stringio'
require 'logger'
class Pricing::GetExtraOne < Pricing

  def bundle_number
    @options[:bundle]
  end

  def unit_price
    @options[:price]
  end

  def extra_one_pricing
    @options[:extra_one_pricing]
  end

  def total quantity
    bundles = quantity / (bundle_number + 1)
    units = quantity % (bundle_number + 1)

    bundled_total(bundles * bundle_number) +
    extra_one_pricing.total(bundles) +
    spare_total(units)
  end

  def bundled_total(bundled_quantity)
    log "Get Extra One: #{bundled_quantity} units @ #{unit_price}€ = #{bundled_quantity * unit_price}€"
    bundled_quantity * unit_price
  end

  def spare_total(units)
    log "#{units} units @ #{unit_price}€ = #{units * unit_price}€"
    units * unit_price
  end

end
