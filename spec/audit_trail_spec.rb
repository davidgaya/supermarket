require 'rspec'
require 'pricing'

describe "Price AuditTrail" do

  it "logs pricing decisions for composite pricing" do
    file = StringIO.new
    audit_log = Logger.new file

    free = Pricing.factory(:unit, price: 0.0, audit: audit_log)
    buy_two_get_one_free = Pricing.factory(:get_extra_one, bundle: 2, price: 1.00, extra_one_pricing: free, audit: audit_log)
    christmas_pricing = Pricing.factory(:evented,
                                        start_date: Date.parse('2014-12-08'),
                                        end_date: Date.parse('2015-01-10'),
                                        pricing: buy_two_get_one_free,
                                        audit: audit_log
    )
    expect(christmas_pricing.total(10)).to eq(7.0)

    file.rewind
    log = file.read
    expect(log).to match("Evented: 2014-12-08>2015-01-10")
    expect(log).to match("Get Extra One: 6 units @ 1.0€ = 6.0€")
  end

end
