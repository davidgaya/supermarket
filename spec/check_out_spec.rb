require 'rspec'
require 'check_out'

describe CheckOut do

  it "takes a pricing rules parameter that responds to pricing form an item" do
    pricing_rules = double("PricingRules")
    item_A = double("Item", sku: 'A')
    expect(pricing_rules).to receive(:total).with(sku: 'A', quantity: 2) { 1.0 }

    co = CheckOut.new(pricing_rules)
    co.scan(item_A)
    co.scan(item_A)

    price = co.total
    expect(price).to eq(1.0)
  end

  it "scans a B , an A , and another B , and recognizes the two B ’s and price them at 45 (for a total price so far of 95 )" do
    pricing_rules = double("PricingRules")
    item_A = double("Item", sku: 'A')
    item_B = double("Item", sku: 'B')
    expect(pricing_rules).to receive(:total).with(sku: 'A', quantity: 1) { 50.0 }
    expect(pricing_rules).to receive(:total).with(sku: 'B', quantity: 2) { 45.0 }

    co = CheckOut.new(pricing_rules)
    co.scan(item_B)
    co.scan(item_A)
    co.scan(item_B)

    price = co.total
    expect(price).to eq(95.0)
  end

end
