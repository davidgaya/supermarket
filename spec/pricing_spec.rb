require 'rspec'
require 'date'
require 'pricing'
require 'system_units/imperial/weight/pound'
require 'system_units/imperial/weight/ounce'

describe Pricing do

  it "should have simple pricing" do
    pricing = Pricing.factory(:unit, price: 0.65)
    expect(pricing.total(1)).to eq(0.65)
    expect(pricing.total(2)).to eq(1.30)
  end

  it "should have bundled pricing" do
    pricing = Pricing.factory(:bundled, bundle: 3, price: 1.00)
    expect(pricing.total(3)).to eq(1.00)
    expect(pricing.total(6)).to eq(2.00)
  end

  it "should have bundled pricing for any quantity" do
    unit_pricing = Pricing.factory(:unit, price: 0.60)
    pricing = Pricing.factory(:bundled, bundle: 3, price: 1.00, unit_pricing: unit_pricing)
    expect(pricing.total(4)).to eq(1.60)
  end

  it "should have uncountable pricing" do
    pricing = Pricing.factory(:uncountable, price: 1.99, per_unit: :pound)
    one_pound = Pound.new(1)
    expect(pricing.total(one_pound)).to eq(1.99)
    four_ounces = Ounce.new(4)
    expect(pricing.total(four_ounces)).to eq(0.4975)
  end

  it "should have get_one_free pricing" do
    free = Pricing.factory(:unit, price: 0.0)
    pricing = Pricing.factory(:get_extra_one, bundle: 2, price: 1.00, extra_one_pricing: free)
    expect(pricing.total(2)).to eq(2.0)
    expect(pricing.total(3)).to eq(2.0)
    expect(pricing.total(4)).to eq(3.0)
    expect(pricing.total(5)).to eq(4.0)
    expect(pricing.total(6)).to eq(4.0)
  end

  it "should have evented pricing" do
    unit_pricing = Pricing.factory(:unit, price: 0.60)
    christmas_pricing = Pricing.factory(:evented,
                                        start_date: Date.parse('2014-12-08'),
                                        end_date: Date.parse('2015-01-10'),
                                        pricing: unit_pricing
    )
    expect(christmas_pricing.total(10)).to eq(6.0)

    allow(Date).to receive(:today).and_return(Date.parse('2015-02-01'))
    christmas_pricing = Pricing.factory(:evented,
                                        start_date: Date.parse('2014-12-08'),
                                        end_date: Date.parse('2015-01-10'),
                                        pricing: unit_pricing
    )
    expect{ christmas_pricing.total(10) }.to raise_error
  end

end
